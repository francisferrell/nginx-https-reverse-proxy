#!/bin/bash

set -euo pipefail

cat >>/etc/ssl/openssl.cnf <<EOF
[ my ca ]
basicConstraints = critical,CA:TRUE,pathlen:0
keyUsage = critical,any
subjectKeyIdentifier = hash
keyUsage = critical,keyCertSign
EOF

openssl genrsa -out /root/ca.key 2048
openssl req -x509 -new -nodes -extensions 'my ca' -key /root/ca.key -sha256 -days 2 -subj "/C=US/ST=CI/L=runner/O=me/CN=${NGINX_HOST} authority" -out /root/ca.pem

mkdir -p /etc/nginx/ssl
openssl genrsa -out /etc/nginx/ssl/ssl.key 2048
openssl req -new -key /etc/nginx/ssl/ssl.key -subj "/C=US/ST=CI/L=runner/O=me/CN=${NGINX_HOST}" -out /root/nginx.csr

cat >/root/nginx.ext <<EOF
authorityKeyIdentifier=keyid,issuer
keyUsage = critical,digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names

[alt_names]
DNS.1 = ${NGINX_HOST}
EOF

openssl x509 -req -in /root/nginx.csr -CA /root/ca.pem -CAkey /root/ca.key -CAcreateserial -out /etc/nginx/ssl/ssl.crt -days 2 -sha256 -extfile /root/nginx.ext
cat /root/ca.pem >>/etc/nginx/ssl/ssl.crt

