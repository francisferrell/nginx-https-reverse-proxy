
# Nginx HTTPS Reverse Proxy

An nginx-based docker container which provides https, using a self-sign certificate, in front of
another application. This is intended for use in gitlab CI for integration or end-to-end testing
with https.


## Motivation

I wanted to automate testing of an API which uses secure cookies, so I needed https.


# Configuration

Run the container with 2 environment variables:

- `NGINX_HOST` -- this is the hostname that nginx will answer on. nginx will be configured with
  this as the `server_name` and this will also be the Subject and Subject Alternate Name on the
  generated certificate
- `BACKEND_HOST` -- this is the backend that nginx should reverse proxy to, including port.

Note that the certificate that is generated will be self-signed and thus not trusted by the client.
Your test suite will need to explicitly trust the certificate. To make this easier, the container
serves up the generated cert over http at the address `http://$NGINX_HOST/ssl.crt`.


# Gitlab CI Example

```yaml
test:
  services:
    - name: $APP_UNDER_TEST
      alias: myapp
    - name: francisferrell/nginx-https
      alias: app
      variables:
        NGINX_HOST: app
        BACKEND_HOST: myapp:8080
  variables:
    FF_NETWORK_PER_BUILD: "true"
  script:
    - curl http://app/ssl.crt >/usr/local/share/ca-certificates/mine.crt
    - update-ca-certificates
    - ... do some tesitng against https://app ...
```

# Links

- Source: https://gitlab.com/francisferrell/nginx-https-reverse-proxy
- Docker Hub: https://hub.docker.com/r/francisferrell/nginx-https

